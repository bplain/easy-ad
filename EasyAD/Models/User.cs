﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyAD.Models
{
    /** This model is a direct interface to AD
     * 
     */
    public class User
    {
        public long Id { get; set; }

        public string username { get; set; }

        public string firstname { get; set; }

        public string lastname { get; set; }

        public string calledby { get; set; }

        public string sid { get; set; }

        public string email { get; set; }

        public string distinguishedname { get; set; }


    }
}